// Package main is a program for a Build Your Own Adventure story
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

// Arc is the structure to represent the JSON objects
type Arc struct {
	Title   string   `json:"title"`
	Story   []string `json:"story"`
	Options []struct {
		Text string `json:"text"`
		Arc  string `json:"arc"`
	} `json:"options"`
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.html"))
}

func main() {
	serve := flag.Bool("Serve", true, "Setting to false runs the program through the command line")
	flag.Parse()
	parseJSON("./gopher.json")
	if *serve == true {
		http.Handle("/", &countHandler{})
		http.ListenAndServe(":8080", nil)
	} else {
		commandLine("intro")
	}
}

func commandLine(key string) {

	arc := arcs[key]

	if arc.Title != "" {
		fmt.Println(arc.Title)
		fmt.Println("")
	}

	for _, paragraph := range arc.Story {
		fmt.Println(paragraph)
		fmt.Println("")
	}

	if len(arc.Options) > 0 {
		for i, option := range arc.Options {
			fmt.Println(i+1, ": ", option.Text)
		}

		fmt.Println("\nEnter number for path")
		inputReader := bufio.NewReader(os.Stdin)
		choice, err := inputReader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
		}

		choice = choice[:len(choice)-1]
		digit, err := strconv.Atoi(choice)
		if err != nil {
			fmt.Println(err)
		}
		commandLine(arc.Options[digit-1].Arc)
	}

}

var arcs map[string]Arc

type countHandler struct {
	mu sync.Mutex
	n  int
}

func (h *countHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	h.mu.Lock()
	defer h.mu.Unlock()
	h.n++

	lastSlash := strings.LastIndex(req.URL.Path, "/") + 1
	arc := arcs[req.URL.Path[lastSlash:]]

	err := tpl.ExecuteTemplate(w, "index.html", arc)
	if err != nil {
		panic(err)
	}
}

func parseJSON(path string) {

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&arcs)
	if err != nil {
		panic(err)
	}
}
