// Package main asks users to solve questions
package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

func main() {

	questionsPath := flag.String("questions", "problems.csv", "A csv file of questions")
	flag.Parse()

	file, err := os.Open(*questionsPath)
	if err != nil {
		log.Panicln(err)
	}

	csvReader := csv.NewReader(file)

	questions, err := csvReader.ReadAll()
	if err != nil {
		log.Panicln(err)
	}

	var score int
	inputReader := bufio.NewReader(os.Stdin)

	fmt.Println("Press enter to begin the timed quiz")
	inputReader.ReadString('\n')

	timer := time.AfterFunc(time.Second*30, func() {
		fmt.Println("Timer expired")
		fmt.Println("You answered", score, "out of", len(questions), "correctly.")
		os.Exit(1)
	})

	for _, question := range questions {

		timer.Reset(time.Second * 30)

		fmt.Println("What is", question[0], "?")
		answer, _ := inputReader.ReadString('\n')

		if question[1] == strings.TrimRight(answer, "\n") {
			score++
		}

		timer.Stop()
	}

	fmt.Println("You answered", score, "out of", len(questions), "correctly.")
	os.Exit(0)
}
