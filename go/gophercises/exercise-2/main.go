package main

import (
	"flag"
	"fmt"
	urlshort "learning/gophercises/exercise-2/handler"
	"net/http"
)

func main() {

	yamlPath := flag.String("YAML", "paths.yaml", "A YAML file containing redirects")
	jsonPath := flag.String("JSON", "paths.json", "A JSON file containing redirects")
	// The DB is running in a docker container.  The IP will need to be swapped out for
	// your specific container's IP address
	dbPath := flag.String("DB", "mongodb://172.19.0.3:27017", "A DB URL")
	flag.Parse()

	mux := defaultMux()

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	mapHandler := urlshort.MapHandler(pathsToUrls, mux)

	// Build the YAMLHandler using the mapHandler as the fallback
	yamlHandler, err := urlshort.YAMLHandler(yamlPath, mapHandler)
	if err != nil {
		panic(err)
	}

	// Build the JSONHandler using the YAMLHandler as the fallback
	jsonHandler, err := urlshort.JSONHandler(jsonPath, yamlHandler)
	if err != nil {
		panic(err)
	}

	// Build the DBHandler using the JSONHandler as the fallback
	dbHandler, err := urlshort.DBHandler(dbPath, jsonHandler)
	if err != nil {
		panic(err)
	}

	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080", dbHandler)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}
