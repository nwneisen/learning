package urlshort

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/yaml.v2"
)

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {

	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if pathsToUrls[r.URL.Path] != "" {
			http.Redirect(rw, r, pathsToUrls[r.URL.Path], http.StatusFound)
		}

		fallback.ServeHTTP(rw, r)
	})
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yamlPath *string, fallback http.Handler) (http.HandlerFunc, error) {

	var redirects []struct {
		Path string `yaml:"path"`
		URL  string `yaml:"url"`
	}

	file, err := os.Open(*yamlPath)
	if err != nil {
		panic(err)
	}

	err = yaml.NewDecoder(file).Decode(&redirects)
	if err != nil {
		return nil, err
	}

	paths := make(map[string]string)
	for _, path := range redirects {
		paths[path.Path] = path.URL
	}

	return MapHandler(paths, fallback), nil
}

// JSONHandler handles routes stored in a JSON
func JSONHandler(jsonPath *string, fallback http.Handler) (http.HandlerFunc, error) {

	var redirects []struct {
		Path string `json:"path"`
		URL  string `json:"url"`
	}

	file, err := os.Open(*jsonPath)
	if err != nil {
		panic(err)
	}

	err = json.NewDecoder(file).Decode(&redirects)
	if err != nil {
		return nil, err
	}

	paths := make(map[string]string)
	for _, path := range redirects {
		paths[path.Path] = path.URL
	}

	return MapHandler(paths, fallback), nil
}

// DBHandler handles routes stored in a database
func DBHandler(dbPath *string, fallback http.Handler) (http.HandlerFunc, error) {

	InitDB(dbPath)
	// To create a path instance in the DB
	// redirect := Redirect{
	// 	"/nix",
	// 	"https://nixstuff.tech",
	// }
	// redirect.Create()

	collection := db.Database("test").Collection("Paths")
	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Panic(err)
	}

	paths := make(map[string]string)
	for cur.Next(context.TODO()) {
		var redirect Redirect
		err := cur.Decode(&redirect)
		if err != nil {
			log.Panic(err)
		}

		paths[redirect.Path] = redirect.URL
	}

	return MapHandler(paths, fallback), nil
}

var db *mongo.Client

// InitDB sets up the database connection
func InitDB(dbPath *string) {

	// Set client options
	clientOptions := options.Client().ApplyURI(*dbPath)

	// Connect to MongoDB
	var err error
	db, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = db.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connected to MongoDB!")
}

// Redirect contains the redirect information
type Redirect struct {
	Path string
	URL  string
}

// Create creates a new credential instance in the DB
func (redirect *Redirect) Create() {

	filter, err := bson.Marshal(redirect)
	if err != nil {
		log.Println(err)
	}

	collection := db.Database("test").Collection("Paths")
	_, err = collection.InsertOne(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}
}
