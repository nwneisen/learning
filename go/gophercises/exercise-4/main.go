package main

import (
	"fmt"
	"os"
	"strings"

	"golang.org/x/net/html"
)

// Link struct for links parsed from html
type Link struct {
	Href string
	Text string
}

func main() {
	doc := getDoc("./ex2.html")
	links := parse(doc)
	for _, link := range links {
		fmt.Println(link)
	}
}

func getDoc(filePath string) *html.Node {
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	doc, err := html.Parse(file)
	if err != nil {
		panic(err)
	}
	return doc
}

func parse(n *html.Node) []Link {
	var links []Link
	if n.Type == html.ElementNode && n.Data == "a" {
		link := Link{
			Href: "",
			Text: strings.TrimSpace(n.FirstChild.Data),
		}
		for _, a := range n.Attr {
			if a.Key == "href" {
				link.Href = a.Val
			}
		}
		links = append(links, link)
	}
	for child := n.FirstChild; child != nil; child = child.NextSibling {
		links = append(links, parse(child)...)
	}
	return links
}
