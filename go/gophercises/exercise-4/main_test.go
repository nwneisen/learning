package main

import (
	"testing"
)

func Test_ex1(t *testing.T) {
	doc := getDoc("ex1.html")
	links := parse(doc)
	if links[0].Href != "/other-page" {
		t.Error("Incorrect Href value at index 0")
	}
	if links[0].Text != "A link to another page" {
		t.Error("Incorrect Text value at index 0")
	}
}

func Test_ex2(t *testing.T) {
	doc := getDoc("ex2.html")
	links := parse(doc)
	if links[0].Href != "https://www.twitter.com/joncalhoun" {
		t.Error("Incorrect Href value at index 0")
	}
	if links[0].Text != "Check me out on twitter" {
		t.Error("Incorrect Text value at index 0")
	}
	if links[1].Href != "https://github.com/gophercises" {
		t.Error("Incorrect Href value at index 1")
	}
	if links[1].Text != "Gophercises is on" {
		t.Error("Incorrect Text value at index 1")
	}
}

func Test_ex3(t *testing.T) {
	doc := getDoc("ex3.html")
	links := parse(doc)
	if links[0].Href != "#" {
		t.Error("Incorrect Href value at index 0")
	}
	if links[0].Text != "Login" {
		t.Error("Incorrect Text value at index 0")
	}
	if links[1].Href != "/lost" {
		t.Error("Incorrect Href value at index 0")
	}
	if links[1].Text != "Lost? Need help?" {
		t.Error("Incorrect Text value at index 0")
	}
	if links[2].Href != "https://twitter.com/marcusolsson" {
		t.Error("Incorrect Href value at index 1")
	}
	if links[2].Text != "@marcusolsson" {
		t.Error("Incorrect Text value at index 1")
	}
}

func Test_ex4(t *testing.T) {
	doc := getDoc("ex4.html")
	links := parse(doc)
	if links[0].Href != "/dog-cat" {
		t.Error("Incorrect Href value at index 0")
	}
	if links[0].Text != "dog cat" {
		t.Error("Incorrect Text value at index 0")
	}
}
