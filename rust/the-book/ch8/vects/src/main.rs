fn main() {
    example1();
    example2();
    example3();
    example4();
    example5();
    example6();
    example7();
    example8();
}

fn example1() {
    let mut v: Vec<i32> = Vec::new();
    v.push(5);
    v.push(6);
    v.push(7);
    v.push(8);
}

fn example2() {
    {
        let _v = vec![1, 2, 3];
    }
    // v is not out of scope
    //print!("{}", v);
}

fn example3() {
    let v = vec![1, 2, 3, 4, 5];
    let third: &i32 = &v[2];
    println!("The third element is {}", third);
    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element."),
    }
}

fn example4() {
    let v = vec![1, 2, 3, 4, 5];
    let _does_not_exist = &v[100];
    let _does_not_exist = v.get(100);
}

fn example5() {
    let mut _v = vec![1, 2, 3, 4, 5];

    // The push could reallocate the vector and so a reference is not allowed
    //let first = &v[0];
    //v.push(6);
    //println!("The first element is: {}", first);
}

fn example6() {
    let v = vec![100, 32, 57];
    for i in &v {
        println!("{}", i)
    }
}

fn example7() {
    let mut v = vec![100, 32, 57];
    for i in &mut v {
        *i += 50;
    }
}

fn example8() {
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let _row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
}
