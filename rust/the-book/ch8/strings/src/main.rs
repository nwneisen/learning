fn main() {
    example1();
    example2();
    example3();
    example4();
    example5();
    example6();
    example7();
    example8();
}

fn example1() {
    let data = "initial contents";
    let _s = data.to_string();
    // Used directly on a string literal
    let _s = "initial contents".to_string();
    let _s = String::from("Initial contents");
}

fn example2() {
    let mut s = String::from("foo");
    s.push_str("bar");
    println!("{}", s);

    let mut s1 = String::from("foo");
    let s2 = "bar";
    s1.push_str(s2);
    println!("s2 is {}", s2);

    let mut s = String::from("lo");
    s.push('l');
    println!("{}", s);
}

fn example3() {
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2;
    println!("{}", s3);
}

fn example4() {
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = format!("{}-{}-{}", s1, s2, s3);
    println!("{}", s);
}

fn example5() {
    let s1 = String::from("hello");
    // String cannot be indexed
    //let h = s1[0];
    println!("{}", s1);
}

fn example6() {
    let len = String::from("Hola").len();
    println!("{}", len);
    let len = String::from("Здравствуйте").len();
    println!("{}", len);

    let _hello = "Здравствуйте";
    // UTF-8 characters take 2 bytes and so what should the follow answer be?
    //let answer = &hello[0];
}

fn example7() {
    let hello = "Здравствуйте";
    let s = &hello[0..4];
    println!("{}", s);
}

fn example8() {
    for c in "नमस्ते".chars() {
        println!("{}", c);
    }

    for b in "नमस्ते".bytes() {
        println!("{}", b);
    }
}
