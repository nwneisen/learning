fn main() {
    section2();
}

fn _section1() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}

fn section2() {
    let x = 5;
    let x = x+1;
    let x = x*2;
    println!("The value of x is {}", x);
}