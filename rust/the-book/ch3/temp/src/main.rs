fn main() {
    let far = 28.0;
    let cel = (far - 32.0) * (5.0/9.0);
    println!("{} degrees Fahrenheit is {} degree Celcius", far, cel);
}
