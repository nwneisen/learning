fn main() {
    _example8();
}

fn _example1() {
    let _x = 2.0;
    let _y: f32 = 60.0;
}
 fn _example2() {
    let _sum = 5 + 10;
    let _difference = 95.5 - 4.3;
    let _product = 4 * 30;
    let _quotient = 56.7/32.3;
    let _remainder = 43 % 5;
}

fn _example3() {
    let _t = true;
    let _f: bool = false;
}
 fn _example4() {
     let _c = 'z';
     let _z = 'Z';
     let _heart_eyed_cat = 't';
 }

fn _example5() {
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    let (_x, y, _z) = tup;
    println!("The value of y is: {}", y);
}

fn _example6() {
    let x: (i32, f64, u8) = (500, 6.4, 1);
    let _five_hundred = x.0;
    let _six_point_four = x.1;
    let _one = x.2;
}

fn _example7() {
    let _a = [1,2,3,4,5];
    let _monthes = ["jan", "feb", "mar", "jun", "jul", "aug", "sept", "nov", "dec"];
    let _a: [i32; 5] = [1,2,3,4,5];
    let _a = [3; 5];
}

fn _example8() {
    let a = [1,2,3,4,5];
    let _first = a[0];
    let _second = a[1];
    let index = 10;
    let element = a[index];
    println!("The value of element is: {}", element);
}
