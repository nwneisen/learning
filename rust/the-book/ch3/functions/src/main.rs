fn main() {
    println!("Hello, world!");
    another_function();
    yet_another_function(5, 6);
    example3();

    let x = five();
    println!("The value of x is: {}", x);

    let x = plus_one(5);
    println!("The new value of x is: {}", x);
}

fn another_function() {
    println!("Another function");
}

fn yet_another_function(x: i32, y: i32) {
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}

fn example3(){
    let _x = 5;
    let y = {
        let _x = 3;
        _x + 1
    };
    println!("Thevalue of y is: {}", y);
} 

fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32 {
    x + 1;
}
