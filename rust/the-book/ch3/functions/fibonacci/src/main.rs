fn main() {
    println!("The nth fibonacci is: {}", fib(7));
}

fn fib(seq: i32) -> i32 {
    if seq == 1 {
        return 1;
    }
    if seq == 2 {
        return 1;
    }

    return fib(seq-1) + fib(seq-2);
}

// 1,1,2,3,5,8,13
// 1,2,3,4,5,6,7
