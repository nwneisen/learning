fn main() {}

fn _example1() {
    let mut s = String::from("hello world");
    let _word = _first_word(&s);
    s.clear();
    //println!("the first word is: {}", _word);
}

fn _first_word(s: &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}

fn _example2() {
    let my_string = String::from("hello world");
    let _word = _first_word(&my_string[..]);
    let my_string_literal = "hello world";
    let _word = _first_word(&my_string_literal[..]);
    let _word = _first_word(my_string_literal);
}
