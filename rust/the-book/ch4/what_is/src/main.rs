fn main() {
    _example7();
}

fn _example1() {
    let mut s = String::from("hello");
    s.push_str(", world!"); // push_str() appends a literal to a String
    println!("{}", s); // This will print 'hello world!'
    {
        let _s = String::from("hello");
    }
}

fn _example2() {
    let x = 5;
    let _y = x;
}

//fn _example3() {
    //let s1 = String::from("hello");
    //let _s2 = s1;
    //println!("The value of s1 is: {}", s1);
//}

fn _example4() {
    let s1 = String::from("hello");
    let s2 = s1.clone();

    println!("s1 = {}, s2 = {}", s1, s2);
}

fn _example5() {
    let s = String::from("hello");
    _takes_ownership(s);
    let x = 5;
    _makes_copy(x);
}

fn _takes_ownership(some_string: String) {
    println!("{}", some_string);
}

fn _makes_copy(some_integer: i32) {
    println!("{}", some_integer);
}

fn _example4_5() {
    let s1 = _gives_ownership();
    println!("{}", s1);
    let s2 = String::from("hello");
    let s3 = _takes_and_gives_back(s2);
    println!("{}", s3);
}

fn _gives_ownership() -> String {
    let some_string = String::from("hello");
    some_string
}

fn _takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn _example7() {
    let s1 = String::from("hello");
    let (s2, len) = _calculate_length(s1);
    println!("The length of '{}' is {}.", s2, len);
}

fn _calculate_length(s: String) -> (String, usize) {
    let length = s.len();
    (s, length)
}


