fn main() {
    _example3();
}

fn _example1() {
    let s1 = String::from("hello");
    let len = _calculate_length(&s1);
    println!("The length of '{}' is {}.", s1, len);
}

fn _calculate_length(s: &String) -> usize {
    s.len()
}

fn _example2() {
    let mut s = String::from("hello");
    _change(&mut s);
}

fn _change(some_string: &mut String) {
    some_string.push_str(", world");
}

fn _example3() {
    let _reference_to_nothing = _dangle();
}

fn _dangle() -> String {
    let s = String::from("hello");
    s
}
