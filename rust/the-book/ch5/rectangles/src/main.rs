fn main() {
    _example3();
}

fn _example1() {
    let width1 = 30;
    let height1 = 50;

    println!(
        "sThe area of the rectangle is {} square pixels.",
        _area1(width1, height1)
    );
}

fn _area1(width: u32, height: u32) -> u32 {
    width * height
}

fn _example2() {
    let rect1 = (30, 50);
    println!(
        "The area of the rectangle is {} square pixels.",
        _area2(rect1)
    );
}

fn _area2(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

fn _example3() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!("rect1 is {:#?}", rect1);

    println! {
        "The area of the rectangle is {} square pixels.",
        _area3(&rect1)
    };
}

fn _area3(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}
