fn main() {
    _structures_1();
}

fn _structures_1() {
    #[derive(Debug)]
    struct Person<'a> {
        // The 'a defines a lifetime
        name: &'a str,
        age: u8,
    }

    // A unit struct
    struct Nil;

    // A tuple struct
    struct Pair(i32, f32);

    // A struct with two fields
    struct Point {
        x: f32,
        y: f32,
    }

    // Structs can be reused as fields of another struct
    struct Rectangle {
        // A rectangle can be specified by where the top left and bottom right
        // corners are in space.
        top_left: Point,
        bottom_right: Point,
    }
    // Create struct with field init shorthand
    let name = "Peter";
    let age = 27;
    let peter = Person { name, age };

    // Print debug struct
    println!("{:?}", peter);

    // Instantiate a 'Point'
    let point: Point = Point { x: 10.3, y: 0.4 };

    // Access the fields of the point
    println!("point coordinates: ({}, {})", point.x, point.y);

    // Make a new point by using struct update syntax to use the fields of out
    // other one
    let bottom_right = Point { x: 5.2, ..point };

    // 'bottom_right.y' will be the same as 'point.y' because we used that field
    // from 'point'
    println!("second point: ({}, {})", bottom_right.x, bottom_right.y);

    // Destructure the point using a 'let' binding
    let Point {
        x: top_edge,
        y: left_edge,
    } = point;

    let rectangle = Rectangle {
        // struct instantiation is an expression too
        top_left: Point {
            x: left_edge,
            y: top_edge,
        },
        bottom_right: bottom_right,
    };

    // Instantiate a unit struct
    let _nil = Nil;

    // Instantiate a tuple struct
    let pair = Pair(1, 0.1);

    // Access the fields of a tuple struct
    println!("pair contains {:?} and {:?}", pair.0, pair.1);

    // Destructure a tuple struct
    let Pair(integer, decimal) = pair;

    println!("pair contains {:?} and {:?}", integer, decimal);

    // Rect area
    let Rectangle {
        top_left: Point { x: left, y: top },
        bottom_right: Point {
            x: right,
            y: bottom,
        },
    } = rectangle;

    let width = right - left;
    let height = top - bottom;

    println!("Area: {}", width * height);

    // Square
    let length: f32 = 5.0;
    let square = Rectangle {
        top_left: Point {
            x: point.x,
            y: point.y + length,
        },
        bottom_right: Point {
            x: point.x + length,
            y: point.y,
        },
    };
    println!("Square: ({}, {}) ({}, {})", square.top_left.x, square.top_left.y, square.bottom_right.x, square.bottom_right.y);
}
