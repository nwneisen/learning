use std::net::TcpListener;

fn main() {
    let listener = TcpListener::bind("localhost:8000").unwrap();
    for stream in listener.incoming() {
        let _stream = stream.unwrap();
        println!("A connection was made");
    }
}
