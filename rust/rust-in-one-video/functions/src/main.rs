fn main() {
    hello_world("Nick");
    add(5, 5);
}

fn hello_world(name: &str) {
    println!("hello your {}", name);
}

fn add(x: i8, y:i8) {
    println!("{}", x+y);
}