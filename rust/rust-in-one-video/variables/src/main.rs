fn main() {
    let x = 1;
    let y = 2.0;
    let nick = "Nick is a string";
    let nickRules = true;

    let dynamicMath = 8*8;

    let myArray = [1,2,3,4,5,6];

    let myTuple = (5, 6.0, "test");

    println!("{}",myTuple.0);
}
