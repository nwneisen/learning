package main

import (
	"log"
	"net/http"
	"text/template"
)

// ParseGlob in an init function
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("starting-files/templates/*.gohtml"))
}

// Use HandleFunc for each of the routes
func main() {

	http.HandleFunc("/", index)
	http.HandleFunc("/about", about)
	http.HandleFunc("/apply", apply)
	http.HandleFunc("/contact", contact)

	http.ListenAndServe(":8080", nil)
}

func about(w http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(w, "about.gohtml", nil)
	if err != nil {
		log.Fatalln(err)
	}
}

// Combine apply & applyProcess into one func called "apply"
// Inside the func "apply", use this code to create the logic to respond differently to a POST method and a GET method
func apply(w http.ResponseWriter, req *http.Request) {

	switch req.Method {

	case http.MethodGet:
		err := tpl.ExecuteTemplate(w, "apply.gohtml", nil)

		if err != nil {
			log.Fatalln(err)
		}

	case http.MethodPost:
		err := tpl.ExecuteTemplate(w, "applyProcess.gohtml", nil)

		if err != nil {
			log.Fatalln(err)
		}
	}
}

func contact(w http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(w, "contact.gohtml", nil)
	if err != nil {
		log.Fatalln(err)
	}
}

func index(w http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(w, "index.gohtml", nil)
	if err != nil {
		log.Fatalln(err)
	}
}
