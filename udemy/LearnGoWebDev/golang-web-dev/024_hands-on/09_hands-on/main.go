package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("templates/index.gohtml"))
}

func main() {

	http.HandleFunc("/", index)
	fs := http.FileServer(http.Dir("public"))
	sp := http.StripPrefix("/public", fs)
	http.Handle("/public/", sp)
	http.ListenAndServe(":8080", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	err := tpl.Execute(w, "index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
}
