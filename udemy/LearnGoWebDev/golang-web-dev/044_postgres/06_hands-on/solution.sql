CREATE DATABASE business;

CREATE TABLE employees
(
    ID SERIAL PRIMARY KEY NOT NULL,
    NAME TEXT NOT NULL,
    SCORE INT DEFAULT 10,
    SALARY INT
);

INSERT INTO 
employees (NAME, SCORE, SALARY) 
VALUES  ('Myke', 27, 58000),
        ('Daniel', 23, 55000),
        ('Arin', 25, 65000),
        ('Juan', 24, 72000),
        ('Shen', 26, 64000),
        ('Myke', 27, 58000),
        ('McLeod', 26, 72000),
        ('James', 32, 35000);

