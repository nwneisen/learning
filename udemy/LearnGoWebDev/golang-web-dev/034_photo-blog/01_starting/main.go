package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	uuid "github.com/satori/go.uuid"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/upload", upload)
	http.Handle("/images/", http.StripPrefix("/images", http.FileServer(http.Dir("./images"))))
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func index(w http.ResponseWriter, req *http.Request) {

	cookie := getCookie(w, req)
	xs := strings.Split(cookie.Value, "|")
	tpl.ExecuteTemplate(w, "index.gohtml", xs)
}

func upload(w http.ResponseWriter, req *http.Request) {

	cookie := getCookie(w, req)
	if req.Method == http.MethodPost {
		file, handler, err := req.FormFile("image")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()

		f, err := os.OpenFile("./images/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer f.Close()

		io.Copy(f, file)
		checkAndAppend(w, cookie, handler.Filename)
	}
	http.Redirect(w, req, "/", http.StatusPermanentRedirect)
}

func getCookie(w http.ResponseWriter, req *http.Request) *http.Cookie {

	cookie, err := req.Cookie("UUID")
	if err != nil {

		id, err := uuid.NewV4()
		if err != nil {
			log.Println(err)
		}
		cookie = &http.Cookie{
			Name:  "UUID",
			Value: id.String(),
		}
		http.SetCookie(w, cookie)
	}

	return cookie
}

func checkAndAppend(w http.ResponseWriter, cookie *http.Cookie, image string) {

	if !strings.Contains(cookie.Value, image) {
		cookie.Value += "|" + image
		http.SetCookie(w, cookie)
	}
}
