package main

import (
	"encoding/json"
	"fmt"
)

type status struct {
	Code    int    `json:"Code"`
	Descrip string `json:"Descrip"`
}

func main() {
	var data []status
	rcvd := `[{"Code":200,"Descrip":"StatusOK"},{"Code":301,"Descrip":"StatusMovedPermanently"},{"Code":302,"Descrip":"StatusFound"}]`

	err := json.Unmarshal([]byte(rcvd), &data)
	if err != nil {
		fmt.Println(err)
	}

	for _, code := range data {
		fmt.Println(code)
	}
}
