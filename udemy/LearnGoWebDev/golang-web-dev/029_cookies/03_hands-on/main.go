package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func main() {

	http.HandleFunc("/", root)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func root(w http.ResponseWriter, req *http.Request) {

	var count int

	visits, err := req.Cookie("Visits")
	if err != nil {
		log.Println(err)
	}

	if err != http.ErrNoCookie {
		count, err = strconv.Atoi(visits.Value)
		if err != nil {
			log.Fatalln(err)
		}
	}

	count++

	http.SetCookie(w, &http.Cookie{
		Name:  "Visits",
		Value: strconv.Itoa(count),
		Path:  "/",
	})

	fmt.Fprintln(w, "Visits count", count)
}
