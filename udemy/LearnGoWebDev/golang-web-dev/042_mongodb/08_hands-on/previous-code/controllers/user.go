package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"udemy/LearnGoWebDev/golang-web-dev/042_mongodb/08_hands-on/previous-code/models"

	"github.com/julienschmidt/httprouter"
)

const databaseFile = "database.json"

type UserController struct {
	database map[string]models.User
}

func NewUserController() *UserController {

	userController := UserController{
		map[string]models.User{},
	}

	if _, err := os.Stat("./" + databaseFile); err == nil {
		fmt.Println("Database file found")
		ur, err := ioutil.ReadFile("./" + databaseFile)
		if err != nil {
			panic(err)
		}

		database := map[string]models.User{}
		if err = json.Unmarshal(ur, &database); err != nil {
			fmt.Println(err)
		}
		userController.database = database
	}

	fmt.Println(userController.database)

	return &userController
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Grab id
	id := p.ByName("id")

	// Fetch user
	u, ok := uc.database[id]
	if !ok {
		w.WriteHeader(404)
		return
	}

	// Marshal provided interface into JSON structure
	uj, _ := json.Marshal(u)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u)

	// create bson ID
	u.ID = "1"

	uc.database[u.ID] = u

	uj, _ := json.Marshal(uc.database)
	err := ioutil.WriteFile(databaseFile, uj, 0644)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated) // 201
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	// Delete user
	delete(uc.database, id)

	uj, _ := json.Marshal(uc.database)
	err := ioutil.WriteFile(databaseFile, uj, 0644)
	if err != nil {
		fmt.Println(err)
	}

	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprint(w, "Deleted user", id, "\n")
}
