package main

import (
	"html/template"
	"log"
	"os"
)

type item struct {
	Name        string
	Description string
	Price       float64
}

type meal struct {
	Name  string
	Items []item
}

type restaurant struct {
	Name  string
	Meals []meal
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("./*.gohtml"))
}

func main() {
	meals1 := []meal{

		meal{"Breakfast",
			[]item{
				item{"Breakfast Burrito", "Scrambled eggs, peppers, onions, and cheese on a flour tortilla", 3.0},
			},
		},
		meal{"Lunch",
			[]item{
				item{"Tacos", "Seasoned beef with tomatoes, onions, and sour cream on a corn totilla", 3.0},
			},
		},
		meal{"Dinner",
			[]item{
				item{"Lotsa Tacos", "3 Seasoned beef with tomatoes, onions, and sour cream on a corn totilla", 6.0},
			},
		},
	}
	meals2 := []meal{

		meal{"Breakfast",
			[]item{
				item{"Eggs", "Scrambled eggs", 3.95},
			},
		},
		meal{"Lunch",
			[]item{
				item{"CheeseBurger", "1/3 lb ground beef with lettuce, tomato, and onion on a sesame seed bun", 5.0},
			},
		},
		meal{"Dinner",
			[]item{
				item{"Steak", "Prime cut steak cooked to your choosing", 6.0},
			},
		},
	}

	restaurants := []restaurant{
		restaurant{"Mexican Palace", meals1},
		restaurant{"Cheap Americans", meals2},
	}

	err := tpl.Execute(os.Stdout, restaurants)
	if err != nil {
		log.Fatalln(err)
	}
}
