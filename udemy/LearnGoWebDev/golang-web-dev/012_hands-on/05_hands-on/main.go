package main

import (
	"html/template"
	"log"
	"os"
)

type item struct {
	Name        string
	Description string
	Price       float64
}

type meal struct {
	Name  string
	Items []item
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("./*.gohtml"))
}

func main() {
	meals := []meal{

		meal{"Breakfast",
			[]item{
				item{"Breakfast Burrito", "Scrambled eggs, peppers, onions, and cheese on a flour tortilla", 3.0},
			},
		},
		meal{"Lunch",
			[]item{
				item{"Tacos", "Seasoned beef with tomatoes, onions, and sour cream on a corn totilla", 3.0},
			},
		},
		meal{"Dinner",
			[]item{
				item{"Lotsa Tacos", "3 Seasoned beef with tomatoes, onions, and sour cream on a corn totilla", 6.0},
			},
		},
	}

	err := tpl.Execute(os.Stdout, meals)
	if err != nil {
		log.Fatalln(err)
	}
}
