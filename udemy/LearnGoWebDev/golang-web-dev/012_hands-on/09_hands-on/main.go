package main

import (
	"encoding/csv"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("./*.gohtml"))
}

func main() {

	file, err := ioutil.ReadFile("./table.csv")
	if err != nil {
		log.Fatalln(err)
	}

	data := csv.NewReader(strings.NewReader(string(file)))
	records, err := data.ReadAll()

	err = tpl.Execute(os.Stdout, records)
	if err != nil {
		log.Fatalln(err)
	}
}
