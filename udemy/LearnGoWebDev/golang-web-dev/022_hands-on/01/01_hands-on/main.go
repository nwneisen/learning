package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/dog", dog)
	http.HandleFunc("/me", me)

	http.ListenAndServe(":8080", nil)
}

func dog(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "This is the dog")
}

func me(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "This is Nick Neisen")
}
