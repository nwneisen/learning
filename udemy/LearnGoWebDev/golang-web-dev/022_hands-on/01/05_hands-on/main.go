package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("./*.gohtml"))
}

func main() {

	http.Handle("/dog", http.HandlerFunc(dog))
	http.Handle("/me", http.HandlerFunc(me))

	http.ListenAndServe(":8080", nil)
}

func dog(w http.ResponseWriter, req *http.Request) {
	name := "Dog"

	tpl.ExecuteTemplate(w, "index.gohtml", name)
}

func me(w http.ResponseWriter, req *http.Request) {
	name := "Nick Neisen"

	err := tpl.ExecuteTemplate(w, "index.gohtml", name)
	if err != nil {
		log.Fatalln(err)
	}

}
