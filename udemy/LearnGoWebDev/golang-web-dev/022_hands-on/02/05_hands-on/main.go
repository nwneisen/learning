package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
)

func main() {

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln(err)
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalln(err)
		}

		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			line := scanner.Text()
			fmt.Println(line)

			if line == "" {
				break
			}
		}

		// Code now gets to these two lines.
		// "I see you connected is still not printed out anywhere"

		fmt.Println("Code got here.")
		io.WriteString(conn, "I see you connected")

		// If I used defer on this is doesn't get call because of the
		// endless loop
		conn.Close()
	}
}
