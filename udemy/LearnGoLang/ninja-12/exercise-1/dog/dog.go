// Package dog finds dog years from human years
package dog

// Years returns the human years passed in as dog years
func Years(years int) int {
	return years * 7
}
