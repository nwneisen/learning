package main

import "fmt"

func main() {

	records := map[string][]string{
		`bond_james`:      {`Shaken, not stirred`, `Martinis`, `Women`},
		`moneypenny_miss`: {`James Bond`, `Literature`, `Computer Science`},
		`no_dr`:           {`Being evil`, `Ice cream`, `Sunsets`},
	}

	records[`neisen_nick`] = []string{`Craft beer`, `Computer Engineering`}

	for i, record := range records {
		fmt.Println(i, record)
	}
}
