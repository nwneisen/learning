package main

import "fmt"

func main() {
	// 	● start with this slice
	x := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}
	// ● use APPEND & SLICING to get these values here which you should ASSIGN to a
	// variable “y” and then print:
	y := append(x[:3], x[6:]...)
	fmt.Println(y)
}
