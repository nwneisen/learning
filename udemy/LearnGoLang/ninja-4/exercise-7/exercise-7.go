package main

import "fmt"

func main() {
	records := [][]string{
		{"James", "Bond", "Shaken, not stirred"},
		{"Miss", "Moneypenny", "Helloooooo, James."},
	}

	for i, record := range records {
		fmt.Println(i)
		for _, data := range record {
			fmt.Println(data)
		}
	}
}
