package main

import "fmt"

type person struct {
	first string
	last  string
}

func main() {

	p1 := person{
		"Bill",
		"Joe",
	}
	fmt.Println(p1)

	changeMe(&p1)
	fmt.Println(p1)

	p1.changeAgain()
	fmt.Println(p1)
}

func changeMe(p *person) {
	p.first = "Bob"
}

// A pointer receiver can change the value
// A nonpointer receiver cannot change the value
// Both still compiled and ran correctly
func (p *person) changeAgain() {
	p.first = "Billy"
}
