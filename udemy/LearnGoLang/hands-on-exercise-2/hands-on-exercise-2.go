package main

import (
	"fmt"
)

func main() {
	t := oddSum(sum, []int{1, 2, 3, 4, 5, 6, 7, 8, 9}...)
	fmt.Println(t)
}

func sum(x ...int) int {
	n := 0
	for _, v := range x {
		n += v
	}
	return n
}

func oddSum(f func(x ...int) int, y ...int) int {
	var odds []int
	for _, val := range y {
		if val%2 != 0 {
			odds = append(odds, val)
		}
	}
	total := sum(odds...)
	return total
}
