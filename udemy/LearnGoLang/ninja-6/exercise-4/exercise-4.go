package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func main() {

	p := person{
		first: "bob",
		last:  "joe",
		age:   40,
	}
	p.speak()
}

func (p person) speak() {
	fmt.Println(p.first, p.last, "is", p.age)
}
