package main

import "fmt"

func main() {
	digits := []int{1, 2, 3, 4, 5}
	fmt.Println(foo(digits...))
	fmt.Println(bar(digits))
}

func foo(input ...int) int {
	var sum int
	for _, v := range input {
		sum += v
	}

	return sum
}

func bar(input []int) int {
	var sum int
	for _, v := range input {
		sum += v
	}
	return sum
}
