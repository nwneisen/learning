package main

import "fmt"

func main() {

	foo(func() {
		fmt.Println("This is the passed in function")
	})
}

func foo(f func()) {
	f()
}
