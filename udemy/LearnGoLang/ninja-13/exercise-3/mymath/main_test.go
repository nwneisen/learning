package mymath

import (
	"fmt"
	"testing"
)

type test struct {
	input  []int
	output float64
}

func TestCenteredAvg(t *testing.T) {

	tests := []test{
		test{[]int{1, 2, 3, 4, 5, 6}, 3.5},
		test{[]int{2, 3, 4, 5, 6, 7}, 4.5},
	}

	for _, test := range tests {
		v := CenteredAvg(test.input)
		if v != test.output {
			t.Error("Expected", test.output, "got", v)
		}
	}
}

func BenchCenteredAvg(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CenteredAvg([]int{1, 2, 3, 4})
	}
}

func ExampleCenteredAvg() {
	fmt.Println(CenteredAvg([]int{1, 2, 3, 4, 5}))
	// Output
	// 1.8
}
