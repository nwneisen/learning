package starting_code

import (
	"fmt"
	"udemy/LearnGoLang/ninja-13/exercise-13/dog"
)

type canine struct {
	name string
	age  int
}

func main() {
	fido := canine{
		name: "Fido",
		age:  dog.Years(10),
	}
	fmt.Println(fido)
	fmt.Println(dog.YearsTwo(20))
}
