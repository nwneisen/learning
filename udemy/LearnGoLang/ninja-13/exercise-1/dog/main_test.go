package dog

import (
	"fmt"
	"testing"
)

func TestYears(t *testing.T) {
	v := Years(2)
	if v != 14 {
		t.Error("Expected 14 got", v)
	}
}

func TestYearsTwo(t *testing.T) {
	v := YearsTwo(2)
	if v != 14 {
		t.Error("Expected 14 got", v)
	}
}

func ExampleYears() {
	fmt.Println(Years(2))
	// Output
	// 14
}

func ExampleYearsTwo() {
	fmt.Println(YearsTwo(2))
	// Output
	// 14
}

func BenchmarkYears(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Years(2)
	}
}

func BenchmarkYearsTwo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		YearsTwo(2)
	}

}
