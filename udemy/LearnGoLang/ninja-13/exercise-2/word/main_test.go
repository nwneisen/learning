package word

import (
	"fmt"
	"testing"
)

func TestCount(t *testing.T) {
	v := Count("Hello")
	if v != 5 {
		t.Error("Expected 5 got", v)
	}
}

func ExampleCount() {
	fmt.Println(Count("Hello World"))
}

func BenchmarkCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Count("Hello")
	}
}
