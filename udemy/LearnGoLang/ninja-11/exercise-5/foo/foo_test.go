package foo

import "testing"

func TestFoo(t *testing.T) {
	v := Foo()
	if v != "foo" {
		t.Error("Expected foo, got ", v)
	}
}
