package main

import "fmt"

// Create a struct “customErr” which implements the builtin.error interface.
type customErr struct {
	data string
}

// Create a value of type “customErr” and pass it into “foo”.
func main() {
	ce := customErr{
		data: "Important stuff",
	}
	foo(ce)
}

func (ce customErr) Error() string {
	return fmt.Sprintf("Important error: %v", ce.data)
}

// Create a func “foo” that has a value of type error as a parameter.
func foo(e error) {
	fmt.Println(e)
}
