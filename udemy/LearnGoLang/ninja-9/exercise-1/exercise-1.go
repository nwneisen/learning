package main

import (
	"fmt"
	"sync"
)

func main() {

	var wg sync.WaitGroup
	wg.Add(2)
	defer wg.Wait()

	go func() {
		fmt.Println("This is the first function")
		wg.Done()
	}()

	go func() {
		fmt.Println("This is the second function")
		wg.Done()
	}()
}
