package main

import "fmt"

type person struct {
	first string
}

func (p *person) speak() {
	fmt.Println(p.first)
}

type human interface {
	speak()
}

func main() {

	hooman := person{
		first: "Bob",
	}
	// Doesn't work
	// saySomething(hooman)
	saySomething(&hooman)
}

func saySomething(h human) {
	h.speak()
}
