package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	var counter int

	var wg sync.WaitGroup
	wg.Add(100)
	defer wg.Wait()

	for i := 0; i < 100; i++ {
		go func() {
			count := counter
			runtime.Gosched()
			count++
			counter = count
			wg.Done()
		}()
	}

	fmt.Println(counter)
}
