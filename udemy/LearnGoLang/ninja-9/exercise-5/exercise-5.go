package main

import (
	"fmt"
	"sync"
)

func main() {

	var counter int

	var wg sync.WaitGroup
	wg.Add(100)

	var mu sync.Mutex

	for i := 0; i < 100; i++ {
		go func() {
			mu.Lock()
			counter++
			mu.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(counter)
}
