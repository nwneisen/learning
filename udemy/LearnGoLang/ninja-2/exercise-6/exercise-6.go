package main

const (
	thisYear = 2019 + iota
	nextYear
	nextNextYear
	finalYear
)

func main() {
	println(thisYear, nextYear, nextNextYear, finalYear)
}
