package main

import "fmt"

func main() {
	number := 15
	fmt.Printf("%d\t%b\t%#x\n", number, number, number)
}
