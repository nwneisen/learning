package main

func main() {
	g := 1 == 2
	h := 1 <= 2
	i := 1 >= 2
	j := 1 != 2
	k := 1 < 2
	l := 1 > 2

	println(g)
	println(h)
	println(i)
	println(j)
	println(k)
	println(l)
}
