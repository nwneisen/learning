package main

import "fmt"

func main() {

	year := 1989
	for year <= 2019 {
		fmt.Println(year)
		year++
	}
}
