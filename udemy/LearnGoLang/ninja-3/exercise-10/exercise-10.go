package main

import "fmt"

func main() {

	fmt.Println("true && true should be true, actually is: ", true && true)
	fmt.Println("true && false should be false, actually is: ", true && false)
	fmt.Println("true || true should be true, actually is: ", true || true)
	fmt.Println("true || false should be true, actually is: ", true || false)
	fmt.Println("!true should be false, actually is: ", !true)
}
