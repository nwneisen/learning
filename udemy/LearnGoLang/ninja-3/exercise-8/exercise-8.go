package main

import "fmt"

func main() {

	switch {
	case true:
		fmt.Println("This case is true")
	case false:
		fmt.Println("This case is false")
	default:
		fmt.Println("This case can't exist")
	}
}
