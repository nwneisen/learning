package main

import "fmt"

type person struct {
	firstName        string
	lastName         string
	favoriteIceCream []string
}

func main() {
	// 	Take the code from the previous exercise, then store the values of type person in a map with the
	// key of last name. Access each value in the map. Print out the values, ranging over the slice.
	person1 := person{
		firstName: "Bill",
		lastName:  "Nei",
		favoriteIceCream: []string{
			"Vanilla",
			"Chocolate",
		},
	}

	person2 := person{
		firstName: "Jon",
		lastName:  "Doe",
		favoriteIceCream: []string{
			"Peanutbutter",
			"Strawberry",
		},
	}

	people := map[string]person{
		person1.lastName: person1,
		person2.lastName: person2,
	}

	fmt.Println(people)

	for i, hooman := range people {
		fmt.Println(i)
		for _, flavor := range hooman.favoriteIceCream {
			fmt.Println("\t", flavor)
		}
	}
}
