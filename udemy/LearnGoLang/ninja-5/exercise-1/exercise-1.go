package main

import "fmt"

type person struct {
	firstName        string
	lastName         string
	favoriteIceCream []string
}

func main() {

	person1 := person{
		firstName: "Bill",
		lastName:  "Nei",
		favoriteIceCream: []string{
			"Vanilla",
			"Chocolate",
		},
	}

	for _, val := range person1.favoriteIceCream {
		fmt.Println(val)
	}

	person2 := person{
		firstName: "Jon",
		lastName:  "Doe",
		favoriteIceCream: []string{
			"Peanutbutter",
			"Strawberry",
		},
	}

	for _, val := range person2.favoriteIceCream {
		fmt.Println(val)
	}
}
