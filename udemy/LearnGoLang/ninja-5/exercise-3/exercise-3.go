package main

import "fmt"

func main() {

	// Create a new type: vehicle.
	type vehicle struct {
		doors int
		color string
	}

	// Create two new types: truck & sedan.
	type truck struct {
		fourWheel bool
		vehicle
	}
	type sedan struct {
		luxury bool
		vehicle
	}

	// Using the vehicle, truck, and sedan structs:
	chevySilvarado := truck{
		fourWheel: true,
		vehicle: vehicle{
			doors: 4,
			color: "blue",
		},
	}
	hondaAccord := sedan{
		luxury: false,
		vehicle: vehicle{
			doors: 4,
			color: "green",
		},
	}

	// Print out each of these values.
	fmt.Println(chevySilvarado)
	fmt.Println(hondaAccord)

	// Print out a single field from each of these values.
	fmt.Println(chevySilvarado.fourWheel)
	fmt.Println(hondaAccord.luxury)
}
