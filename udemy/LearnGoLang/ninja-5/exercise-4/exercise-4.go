package main

import "fmt"

func main() {

	circle := struct {
		width []int
		color map[string]int
	}{
		width: []int{1, 2, 23, 4, 5},
		color: map[string]int{
			"red": 4,
		},
	}

	fmt.Println(circle.color["red"])
	fmt.Println(circle.width[2:])
}
