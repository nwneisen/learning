package main

import (
	"fmt"
	"sync"
)

// write a program that
// ○ launches 10 goroutines
// ■ each goroutine adds 10 numbers to a channel
// ○ pull the numbers off the channel and print them

func main() {

	out := make(chan int)

	var wg sync.WaitGroup
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go func() {
			for i := 0; i < 10; i++ {
				out <- i
			}
			wg.Done()
		}()
	}

	go func() {
		for v := range out {
			fmt.Println(v)
		}
	}()

	wg.Wait()
	close(out)
}
