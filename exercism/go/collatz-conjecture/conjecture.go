package collatzconjecture

import (
	"errors"
)

// CollatzConjecture counts the steps to get a number to 1
func CollatzConjecture(number int) (int, error) {
	var steps int

	for number != 1 {

		if number <= 0 {
			return number, errors.New("number reached 0")
		}

		if number%2 == 0 {
			number /= 2
		} else {
			number = (number * 3) + 1
		}

		steps++
	}

	return steps, nil
}
