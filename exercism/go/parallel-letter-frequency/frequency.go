// Package letter contains functions for counting runes.
package letter

import (
	"sync"
)

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

// ConcurrentFrequency concurrently counts runes in strings of text.
func ConcurrentFrequency(texts []string) FreqMap {
	frequencies := FreqMap{}
	counted := make(chan FreqMap)

	go spawnRoutines(texts, counted)

	for frequency := range counted {
		for key, value := range frequency {
			frequencies[key] += value
		}
	}

	return frequencies
}

// spawnRoutines kicks off multiple routines for counting runes
func spawnRoutines(texts []string, counted chan<- FreqMap) {
	var wg sync.WaitGroup
	wg.Add(len(texts))
	for _, text := range texts {
		go func(text string) {
			counted <- Frequency(text)
			wg.Done()
		}(text)
	}
	wg.Wait()
	close(counted)
}
