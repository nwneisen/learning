// Package clock contains functions for working with a simple clock.
package clock

import "fmt"

// Clock contains data needed for a simple clock.
type Clock struct {
	minute int
	hour   int
}

// New creates a new Clock value.
func New(hour int, minute int) Clock {
	c := Clock{}

	c = c.Add(hour * 60)
	c = c.Add(minute)
	return c
}

// String outputs the current clock values as a string.
func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c.hour, c.minute)
}

// Add the passed in minutes to the current clock time.
func (c Clock) Add(minutes int) Clock {

	if minutes < 0 {
		return c.Subtract(0 - minutes)
	}

	c.minute += minutes

	if c.minute >= 60 {
		c.hour += c.minute / 60
		c.minute %= 60
	}

	if c.hour >= 24 {
		c.hour %= 24
	}

	return c
}

// Subtract the passed in minutes from the current clock time.
func (c Clock) Subtract(minutes int) Clock {

	if minutes < 0 {
		c = c.Add(0 - minutes)
	}

	c.minute -= minutes

	for c.minute < 0 {
		c.hour--
		c.minute += 60
	}

	for c.hour < 0 {
		c.hour += 24
	}

	return c
}
