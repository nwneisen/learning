// Package isogram contains functions for working with isograms.
package isogram

import (
	"strings"
	"unicode"
)

// IsIsogram checks if the passed in word is an isogram.
func IsIsogram(word string) bool {

	var prevLetters = make(map[rune]bool)
	word = strings.ToLower(word)

	for _, letter := range word {
		if !unicode.IsLetter(letter) {
			continue
		}

		if prevLetters[letter] {
			return false
		}

		prevLetters[letter] = true
	}

	return true
}
