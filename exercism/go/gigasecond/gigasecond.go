package gigasecond

import "time"

// AddGigasecond adds a gigasecond to passed in time
func AddGigasecond(t time.Time) time.Time {
	return t.Add(time.Second * 1000000000)
}
