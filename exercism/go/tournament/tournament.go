// Package tournament contains functions for tournament calculations
package tournament

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strings"
)

type stats struct {
	matches int
	wins    int
	losses  int
	draws   int
	points  int
}

// Tally aggregates tournament match data
func Tally(r io.Reader, w io.Writer) error {
	teams := make(map[string]*stats)
	reader := bufio.NewReader(r)
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return err
		}
		words := strings.Split(string(line), ";")
		if len(words) != 3 {
			return errors.New("Invalid line format")
		}
		for _, word := range words {
			fmt.Println(word)
		}
		if _, ok := teams[words[0]]; ok {
			teams[words[0]] = &stats{}
		}
		// if _, ok := teams[words[1]]; ok {
		// 	teams[words[1]] = &stats{}
		// }
		// if words[2] == "win" {
		// 	teams[words[0]].wins++
		// 	teams[words[1]].losses++
		// }
	}
	return nil
}
