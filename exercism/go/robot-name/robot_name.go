// Package robotname contains code for managing the robot names
package robotname

import (
	"errors"
	"math/rand"
	"time"
)

var used = map[string]bool{}
var possibilities = 26 * 26 * 10 * 10 * 10

// Robot is a structure to hold the generate robot name
type Robot struct {
	name string
}

// Name returns the randomly generated robot name
func (r *Robot) Name() (string, error) {

	if possibilities == 0 {
		return "", errors.New("All possible names used")
	}

	for r.name == "" {
		randomChars := [5]int{0}
		rand.Seed(time.Now().UnixNano())

		randomChars[0] = rand.Intn(26)
		randomChars[1] = rand.Intn(26)
		randomChars[2] = rand.Intn(10)
		randomChars[3] = rand.Intn(10)
		randomChars[4] = rand.Intn(10)

		for i := range randomChars {

			randomChars[i]++

			r.name += string(randomChars[0] + 'A')
			r.name += string(randomChars[1] + 'A')
			r.name += string(randomChars[2] + '0')
			r.name += string(randomChars[3] + '0')
			r.name += string(randomChars[4] + '0')

			if !used[r.name] {
				used[r.name] = true
				possibilities--
				break
			} else {
				r.name = ""
			}
		}
	}

	return r.name, nil
}

// Reset sets the robot's name to an empty string
func (r *Robot) Reset() {
	r.name = ""
}
