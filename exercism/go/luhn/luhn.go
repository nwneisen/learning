// Package luhn contains functions for the Luhn formula
package luhn

import (
	"strings"
)

// Valid checks for a valid number using the Luhn formula
func Valid(input string) bool {

	var sum int

	input = strings.Replace(input, " ", "", -1)

	if len(input) <= 1 {
		return false
	}

	for index, character := range input {
		// If not number, return false
		if character < '0' || character > '9' {
			return false
		}

		digit := int(character) - '0'
		// Check for doubling on every other from the right
		if (len(input)-1-index)%2 == 1 {
			digit *= 2
			// if greater than 9, subtract 9
			if digit > 9 {
				digit -= 9
			}

		}

		// Add to the sum
		sum += digit
	}

	// Check if divisable by 10
	return sum%10 == 0
}
