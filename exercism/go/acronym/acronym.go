package acronym

import (
	"strings"
)

// Abbreviate creates an acronym for the input string
func Abbreviate(s string) string {

	var acronym string

	for _, word := range strings.FieldsFunc(s, Split) {
		acronym += strings.ToUpper(string(word[0]))
	}

	return acronym
}

// Split tells the fields function when specific characters are found
func Split(letter rune) bool {
	return letter == ' ' || letter == '-' || letter == '_'
}
