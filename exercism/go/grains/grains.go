// Package grains contains functions for computing grain count on a chess board
package grains

import (
	"errors"
)

// Square return show many grains for the given square
func Square(input int) (uint64, error) {

	if input < 1 || input > 64 {
		return 0, errors.New("square outside of allowed range")
	}

	return 1 << uint(input-1), nil
}

// Total returns the number of grains for the entire board
func Total() uint64 {
	return 1<<64 - 1
}
