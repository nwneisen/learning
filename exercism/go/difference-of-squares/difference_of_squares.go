// Package diffsquares contains functions for finding the different of squares.
package diffsquares

// SquareOfSum finds the square of sums.
func SquareOfSum(n int) int {

	var sum int

	// Based on summation formula found at
	// https://brilliant.org/wiki/sum-of-n-n2-or-n3/
	sum = (n * (n + 1)) / 2

	return sum * sum
}

// SumOfSquares finds the sum of squares
func SumOfSquares(n int) int {

	// Based on summation formula found at
	// https://brilliant.org/wiki/sum-of-n-n2-or-n3/
	return (n * (n + 1) * (2*n + 1)) / 6
}

// Difference finds the different between two values
func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
