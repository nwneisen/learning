// Package strand continas functions for working with DNA and RNA strings
package strand

import "strings"

var complements = map[rune]rune{
	'G': 'C',
	'C': 'G',
	'T': 'A',
	'A': 'U',
}

// ToRNA converts a DNA string to an RNA string
func ToRNA(dna string) string {

	var rna strings.Builder

	for _, letter := range dna {
		rna.WriteRune(complements[letter])
	}

	return rna.String()
}
