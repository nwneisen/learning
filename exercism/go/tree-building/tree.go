// Package tree has methods for creating a forum tree from records
package tree

import (
	"errors"
	"sort"
)

// Record holds an individual forum post's information
type Record struct {
	ID     int
	Parent int
}

// Node holds forum data in a Node form
type Node struct {
	ID       int
	Children []*Node
}

// Build creates a Node structure from a Record list
func Build(records []Record) (*Node, error) {
	if len(records) == 0 {
		return nil, nil
	}
	sort.Slice(records, func(i int, j int) bool {
		return records[i].ID < records[j].ID
	})
	nodes := make(map[int]*Node, len(records))
	for _, record := range records {
		if record.ID != 0 && nodes[record.ID-1] == nil {
			return nil, errors.New("non-continuous node IDs")
		}
		if record.ID == 0 && record.Parent != 0 {
			return nil, errors.New("root node has parent")
		}
		if nodes[record.ID] != nil {
			return nil, errors.New("multiple nodes")
		}
		nodes[record.ID] = &Node{ID: record.ID}
		if record.ID != 0 {
			if record.ID == record.Parent {
				return nil, errors.New("cycle directly")
			}
			if nodes[record.Parent] == nil {
				return nil, errors.New("cycle indirectly")
			}
			if len(nodes[record.Parent].Children) >= record.ID {
				return nil, errors.New("duplicate parent node")
			}
			nodes[record.Parent].Children = append(nodes[record.Parent].Children, nodes[record.ID])
		}
	}
	return nodes[0], nil
}
