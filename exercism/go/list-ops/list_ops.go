package listops

func Foldr(fn func(int, int) int, initial int) int {
	return 0
}

func Foldl(fn func(int, int) int, initial int) int {
	return 0
}

func Filter(fn func(int) bool) []int {
	return []int{}
}

func Length(list []int) int {
	return 0
}

func Map(fn func(x int) int) []int {
	return []int{}
}

func Reverse(list []int) []int {
	return []int{}
}

func Append(appendThis []int) []int {
	return []int{}
}

// func Concat(args []IntList) []int {
// 	return []int{}
// }
