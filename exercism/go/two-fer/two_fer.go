package twofer

import "strings"

// ShareWith returns 'One for you, one for me.' with the name injected
func ShareWith(name string) string {

	shareWith := "One for you, one for me."

	if name != "" {
		shareWith = strings.Replace(shareWith, "you", name, 1)
	}

	return shareWith
}
