package triangle

import "math"

// Kind is an integar representation of the traingle type
type Kind int

const (
	// NaT is not a triangle
	NaT = 0
	// Equ is an equilateral triangle
	Equ = 1
	// Iso is an isosceles triangle
	Iso = 2
	// Sca is a scalene triangle
	Sca = 3
)

// KindFromSides returns the type of triangle based on the side lengths.
func KindFromSides(a, b, c float64) Kind {

	// Side less than or equal to 0
	if a <= 0 || b <= 0 || c <= 0 {
		return NaT
	}

	// Check for NaN
	if math.IsNaN(a) || math.IsNaN(b) || math.IsNaN(c) {
		return NaT
	}

	if math.IsInf(a, 1) || math.IsInf(b, 1) || math.IsInf(c, 1) {
		return NaT
	}

	// Sum of two sides is smaller than the third
	if a+b < c || a+c < b || b+c < a {
		return NaT
	}

	// Sum of two sides is equal to the third
	// The only test meeting this specification has is marked as a Isosceles triangle
	// if a+b == c || a+c == b || b+c == a {
	// 	return NaT
	// }

	var k Kind

	if a == b && a == c && b == c {
		// Equilatteral
		k = Equ
	} else if a == b || a == c || b == c {
		// Isosceles
		k = Iso
	} else {
		// Scalene
		k = Sca
	}

	return k
}
