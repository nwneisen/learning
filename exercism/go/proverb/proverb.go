// Package proverb contians functions for proverbs
package proverb

import "fmt"

// Proverb outputs a proverb depending on the rhyme input
func Proverb(rhyme []string) []string {

	var proverb []string
	var line string

	for i := 0; i < len(rhyme)-1; i++ {
		line = fmt.Sprintf("For want of a %s the %s was lost.", rhyme[i], rhyme[i+1])
		proverb = append(proverb, line)
	}

	if len(rhyme) > 0 {
		line = fmt.Sprintf("And all for the want of a %s.", rhyme[0])
		proverb = append(proverb, line)
	}

	return proverb
}
