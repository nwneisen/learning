// Package bob contains functions for Bob bot
package bob

import (
	"fmt"
	"regexp"
)

// Hey provides a different response for Bob depending on the remark
func Hey(remark string) string {

	containsUppers, err := regexp.MatchString(`[A-Z]`, remark)
	if err != nil {
		fmt.Println(err)
	}

	containsLowers, err := regexp.MatchString(`[a-z]`, remark)
	if err != nil {
		fmt.Println(err)
	}

	endsInQuestion, err := regexp.MatchString(`\?[ ]*$`, remark)
	if err != nil {
		fmt.Println(err)
	}

	empty, err := regexp.MatchString(`^[ \t\n\r]*$`, remark)
	if err != nil {
		fmt.Println(err)
	}

	if containsUppers && !containsLowers && endsInQuestion {
		return "Calm down, I know what I'm doing!"
	}

	if containsUppers && !containsLowers && endsInQuestion {
		return "Calm down, I know what I'm doing!"
	}

	if containsUppers && !containsLowers {
		return "Whoa, chill out!"
	}

	if endsInQuestion {
		return "Sure."
	}

	if empty {
		return "Fine. Be that way!"
	}

	return "Whatever."
}
