// Package contains functions for working with DNA
package dna

import "errors"

// Histogram represented as a map
type Histogram map[rune]int

// DNA represented as a string
type DNA string

// Counts determines the number of valid nucleotides in a given string
func (d DNA) Counts() (Histogram, error) {
	var h = Histogram{
		'A': 0,
		'C': 0,
		'G': 0,
		'T': 0,
	}

	for _, letter := range d {
		_, ok := h[letter]

		if ok {
			h[letter]++
		} else {
			return h, errors.New("Invalid nucleotide found")
		}
	}

	return h, nil
}
