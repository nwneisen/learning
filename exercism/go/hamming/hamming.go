package hamming

import (
	"errors"
	"unicode/utf8"
)

// Distance finds the Hamming distance between two DNA strands
func Distance(a, b string) (int, error) {

	if utf8.RuneCountInString(a) != utf8.RuneCountInString(b) {
		return 0, errors.New("strand lengths dont match")
	}

	count := 0

	for i := range []rune(a) {
		if []rune(a)[i] != []rune(b)[i] {
			count++
		}
	}

	return count, nil
}
