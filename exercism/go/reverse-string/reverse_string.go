package reverse

import "strings"

// Reverse reverses and input string
func Reverse(input string) string {
	var reversedString strings.Builder

	runeString := []rune(input)
	for i := len(runeString) - 1; i >= 0; i-- {
		reversedString.WriteRune(runeString[i])
	}

	return reversedString.String()
}
