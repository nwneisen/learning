package raindrops

import (
	"bytes"
	"strconv"
)

// Convert changes an input int to a string in raindrop speak
func Convert(number int) string {

	var convertedNumber bytes.Buffer

	if number%3 == 0 {
		convertedNumber.WriteString("Pling")
	}
	if number%5 == 0 {
		convertedNumber.WriteString("Plang")
	}
	if number%7 == 0 {
		convertedNumber.WriteString("Plong")
	}

	if convertedNumber.Len() == 0 {
		convertedNumber.WriteString(strconv.Itoa(number))
	}

	return convertedNumber.String()
}
